mp3rename (0.6-13) unstable; urgency=medium

  * QA upload.
  * debian/control: bumped Standards-Version to 4.6.0.

 -- David da Silva Polverari <david.polverari@gmail.com>  Mon, 20 Sep 2021 17:14:09 +0000

mp3rename (0.6-12) unstable; urgency=medium

  * QA upload.
  * Upload to unstable.

 -- Adrian Bunk <bunk@debian.org>  Thu, 16 Sep 2021 14:24:33 +0300

mp3rename (0.6-11) experimental; urgency=medium

  * QA upload.
  * Migrations:
      - DH to new level format. Consequently:
          ~ debian/compat: removed.
          ~ debian/control: changed from 'debhelper' to 'debhelper-compat' in
            Build-Depends field and bumped level to 13.
      - DebSrc to 3.0.
      - debian/copyright to 1.0 format.
      - debian/rules to new (reduced) format.
  * Ran wrap-and-sort.
  * debian/changelog: removed trailing whitespace and cruft.
  * debian/clean: created to remove auto-generated manpage.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Added the Vcs-* fields.
      - Bumped Standards-Version to 4.5.1.
      - Removed help2man from Build-Depends, as it is no longer needed.
  * debian/copyright: updated upstream and packaging copyright information.
  * debian/manpages: created to install static manpage. Also, along with
    using dh sequencer in debian/rules, fixes FTCBFS. Thanks to Helmut Grohne
    <helmut@subdivi.de> (Closes: #901333).
  * debian/mp3rename.1: created by using help2man and formatted manually.
  * debian/patches/:
      - 010_old-makefile-diffs.patch:
          ~ Added to include all previous Makefile diffs from old Debian format
            packaging.
          ~ Included $(CPPFLAGS) and $(LDFLAGS) as compiler flags to allow
            hardening.
          ~ Using $(DESTDIR) to support staged installs.
      - 020_old-source-diffs.patch: added to include all previous mp3rename.c
        diffs from old Debian format packaging.
      - 030_fix-format-string.patch: added to fix a format string vulnerability.
      - 040_dont_open_rdwr.patch: added to avoid unnecessarily opening files
        RDWR. Thanks to Joey Hess <id@joeyh.name> (Closes: #772199).
      - 050_recognize_dashp_option.patch: added to recognize the -p option.
        Thanks to Shaun Jackman <sjackman@gmail.com> (Closes: #272221, #431297,
        LP: #199853).
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/control: created to perform CI tests.
  * debian/upstream/metadata: created.

 -- David da Silva Polverari <david.polverari@gmail.com>  Tue, 15 Jun 2021 17:06:54 +0000

mp3rename (0.6-10) unstable; urgency=medium

  * QA upload.
  * Ack for NMU.
  * Add {misc:Depends} to Depends field.
  * Bump DH level to 10. (Closes: #800274)
  * Bump Standards-Version to 3.9.8.
  * debian/watch: added a fake site to explain about
    the current status of the original upstream homepage.
  * Set QA Group as maintainer. (see #839972)

 -- Allan Dixon Jr. <allandixonjr@gmail.com>  Sun, 06 Nov 2016 06:14:29 -0500

mp3rename (0.6-9) unstable; urgency=low

  * Apply patch from David Whitmarsh
    - should be able to zero pad the track number so it sorts bett
    (Closes: #272221)
  * Lintian cleanup debian/copyright
  * DEBHELPER compat ver 3

 -- Mark Purcell <msp@debian.org>  Thu,  1 Sep 2005 21:45:05 +0100

mp3rename (0.6-8) unstable; urgency=low

  * Improve Package description (Closes: Bug#209942)

 -- Mark Purcell <msp@debian.org>  Wed, 10 Sep 2003 22:57:43 +1000

mp3rename (0.6-7) unstable; urgency=low

  * Apply ID3v2 patch from Niki "K.-M. Hansche" (Closes: Bug#130459, Bug#91906)
  * lintian cleanup copyright-lists-upstream-authors-with-dh_make-
    boilerplate

 -- Mark Purcell <msp@debian.org>  Sun,  6 Jul 2003 21:49:04 +1000

mp3rename (0.6-6) unstable; urgency=low

  * Fixup help2man path

 -- Mark Purcell <msp@debian.org>  Mon,  8 Apr 2002 20:51:43 +1000

mp3rename (0.6-5) unstable; urgency=low

  * lintian cleanup. Added man page mp3rename(1)

 -- Mark Purcell <msp@debian.org>  Sun,  7 Apr 2002 21:57:30 +1000

mp3rename (0.6-4) unstable; urgency=low

  * Extra else in Gun patch (Closes: Bug#124475)

 -- Mark Purcell <msp@debian.org>  Thu, 27 Dec 2001 21:46:45 +1100

mp3rename (0.6-3) unstable; urgency=low

  * Added Alexander Gun patch to fix track numbers (Closes: Bug#124475)

 -- Mark Purcell <msp@debian.org>  Sun, 23 Dec 2001 20:12:01 +1100

mp3rename (0.6-2) unstable; urgency=low

  * Document usage parameters in README.Debian (Closes: #91902, #91905)
  * Include partial track number support patch. (Closes: #112382) Thanks Marcin Owsiany

 -- Mark Purcell <msp@debian.org>  Mon, 17 Sep 2001 20:50:50 +1000

mp3rename (0.6-1) unstable; urgency=low

  * Initial Release. Closes: bug#90096

 -- Mark Purcell <msp@debian.org>  Fri, 23 Feb 2001 22:28:22 +1100
